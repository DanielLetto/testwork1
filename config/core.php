<?php

return [
    'categoryPathJson' => storage_path() . '/json/category.json',
    'categoryFailPathJson' => storage_path() . '/json/categoryFail_' . date("d_m_Y") . '_.json',

    'productPathJson' => storage_path() . '/json/product.json',
    'productFailPathJson' => storage_path() . '/json/productFail_' . date("d_m_Y") . '_.json'
];
