<?php

namespace App\Listeners\Product;

use App\Events\EProductUpdate;
use App\Mail\ProductMailEvent;
use App\Services\Product\SProductUpdate;
use Illuminate\Support\Facades\Mail;

class ProductUpdateSendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SProductUpdate $event
     * @return void
     */
    public function handle(EProductUpdate $event)
    {
        Mail::to("receiver@example.com")->send(new ProductMailEvent($event->Product));
    }
}
