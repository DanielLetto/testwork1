<?php

namespace App\Listeners\Product;

use App\Events\EProductCreate;
use App\Mail\ProductMailEvent;
use App\Services\Product\SProductCreate;
use Illuminate\Support\Facades\Mail;

class ProductCreateSendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SProductCreate $event
     * @return void
     */
    public function handle(EProductCreate $event)
    {
        Mail::to("receiver@example.com")->send(new ProductMailEvent($event->Product));
    }
}
