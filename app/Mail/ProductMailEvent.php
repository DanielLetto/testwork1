<?php

namespace App\Mail;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductMailEvent extends Mailable
{
    use Queueable, SerializesModels;

    public $Product;

    public function __construct(Product $product)
    {
        $this->Product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sender@example.com')
            ->view('mails.demo');
    }
}
