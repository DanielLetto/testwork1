<?php

namespace App\Handlers\Events;

use App\Events\AddLogs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Log
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param AddLogs $event
     * @return void
     */
    public function handle(AddLogs $event)
    {
        //
    }
}
