<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Services\Category\SCategoryCreate;
use App\Services\Category\SCategoryUpdate;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Validator;


class CategoriesJson extends Command
{
    protected $errorsValidate;
    protected $pathData;
    protected $pathFailData;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:createOrUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param SCategoryCreate $categoryCreate
     * @param SCategoryUpdate $categoryUpdate
     */
    public function __construct()
    {
        parent::__construct();

        $this->pathData = config('core.categoryPathJson');
        $this->pathFailData = config('core.categoryFailPathJson');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $newFailData = [];

        $this->getData()
            ->each(function ($item) use (&$newFailData) {

                $validator = Validator::make(
                    ['title' => $item['title']],
                    ['title' => 'required|string|min:3|max:12']);

                if ($validator->fails()) {
                    $this->errorsValidate++;
                    $newFailData[] = [
                        'data' => $item,
                        'errors' => $validator->errors()
                    ];
                } else {
                    $category = Category::query()->where('title', $item['title'])->first();

                    if ($category) {
                        app(SCategoryUpdate::class)
                            ->init([
                                'id' => $category->id,
                                'title' => $item['title'],
                                'eid' => $item['eId']
                            ])->handle();
                    } else {
                        app(SCategoryCreate::class)
                            ->init([
                                'title' => $item['title'],
                                'eid' => $item['eId']
                            ])->handle();
                    }
                }

            });

        $this->saveFailData($newFailData);

        $this->info('errorsValidate: ' . $this->errorsValidate);

        return true;
    }

    protected function getData(): Collection
    {
        return collect(json_decode(file_get_contents($this->pathData), true));
    }

    protected function getFailData(): array
    {
        $contentFail = [];
        if (file_exists($this->pathFailData)) {
            $contentFail = json_decode(file_get_contents($this->pathFailData), true);
        }
        return $contentFail;
    }

    protected function saveFailData(array $newFailData)
    {
        if ($this->errorsValidate) {
            file_put_contents($this->pathFailData, json_encode(array_merge($this->getFailData(), $newFailData)));
        }
    }
}
