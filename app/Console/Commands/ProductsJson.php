<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Services\Product\SProductCreate;
use App\Services\Product\SProductUpdate;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Validator;

class ProductsJson extends Command
{
    protected $errorsValidate;
    protected $pathData;
    protected $pathFailData;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:createOrUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param SProductCreate $productCreate
     * @param SProductUpdate $productUpdate
     */
    public function __construct()
    {
        parent::__construct();

        $this->pathData = config('core.productPathJson');
        $this->pathFailData = config('core.productFailPathJson');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $newFailData = [];

        $this->getData()
            ->each(function ($item) use (&$newFailData) {

                $validator = Validator::make([
                    'title' => $item['title'],
                    'category_ids' => $item['category_ids'],
                    'price' => $item['price'],
                ], [
                    'category_ids' => 'required|array',
                    'category_ids.*' => 'exists:categories,id',
                    'title' => 'required|string|min:3|max:12',
                    'price' => 'required|min:0|max:200',
                ]);

                if ($validator->fails()) {

                    $this->errorsValidate++;

                    $newFailData[] = [
                        'data' => $item,
                        'errors' => $validator->errors()
                    ];

                } else {

                    $product = Product::query()->where('title', $item['title'])->first();

                    if ($product) {
                        app(SProductUpdate::class)
                            ->init([
                                'id' => $product->id,
                                'title' => $item['title'],
                                'price' => $item['price'],
                                'eid' => $item['eId'],
                                'category_ids' => $item['category_ids']
                            ])->handle();
                    } else {
                        app(SProductCreate::class)
                            ->init([
                                'title' => $item['title'],
                                'price' => $item['price'],
                                'eid' => $item['eId'],
                                'category_ids' => $item['category_ids']
                            ])->handle();
                    }
                }
            });

        $this->saveFailData($newFailData);

        $this->info('errorsValidate: ' . $this->errorsValidate);

        return true;
    }

    protected function getData(): Collection
    {
        return collect(json_decode(file_get_contents($this->pathData), true));
    }

    protected function getFailData(): array
    {
        $contentFail = [];
        if (file_exists($this->pathFailData)) {
            $contentFail = json_decode(file_get_contents($this->pathFailData), true);
        }
        return $contentFail;
    }

    protected function saveFailData(array $newFailData)
    {
        file_put_contents($this->pathFailData, json_encode(array_merge($this->getFailData(), $newFailData)));
    }
}
