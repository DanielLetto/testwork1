<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property int|null $eid
 * @property string $title
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @method static Builder|Category whereEid($value)
 * @method static Builder|Category whereTitle($value)
 * @mixin Eloquent
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 */
class Category extends Model
{
    public $timestamps = false;

    public $attributes = [
        'title' => null,
        'eid' => null,
    ];

    protected $fillable = [
        'title',
        'eid',
    ];

    protected $casts = [
        'title' => 'string',
        'eid' => 'int',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
