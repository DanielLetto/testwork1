<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property int|null $eid
 * @property float $price
 * @property string $title
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCategories($value)
 * @method static Builder|Product whereEid($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereTitle($value)
 * @mixin Eloquent
 * @method static Builder|Product whereCategoryId($value)
 * @property-read Collection|Category[] $categories
 * @property-read int|null $categories_count
 */
class Product extends Model
{
    public $timestamps = false;

    public $attributes = [
        'title' => '',
        'price' => 0,
        'eid' => null,
    ];

    protected $fillable = [
        'title',
        'price',
        'eid',
    ];

    protected $casts = [
        'title' => 'string',
        'price' => 'float',
        'eid' => 'int',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
