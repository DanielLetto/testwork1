<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 2019-04-15
 * Time: 10:30
 */

namespace App\Services;


interface ServiceInterface
{
    public function handle();

    public function validate();

    public function rules();
}
