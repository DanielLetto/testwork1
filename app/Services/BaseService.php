<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 2019-04-13
 * Time: 18:17
 */

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

abstract class BaseService implements ServiceInterface
{
    protected $params;
    protected $rawParams = [];
    protected $errors = [];
    protected $result;
    protected $errorMessages = [];
    protected $rules = [];
    protected $fillableModels = [];


    /**
     * @param array $params
     * @return $this
     * @throws ValidationException
     */
    public function init($params = [])
    {
        $this->params = $params;
        $this->rawParams = $params;
        $this->validate();
        $this->fillModels();
        if (method_exists($this, 'afterInit')) {
            $this->afterInit();
        }
        //добавляем в this->params только то что есть в rules
        $this->params = [];
        foreach ($params as $key => $value) {
            if (isset($this->rules()[$key])) {
                $this->params[$key] = $value;
            }
        }
        return $this;
    }

    public function rules()
    {
        return $this->rules;
    }

    /**
     * @throws ValidationException
     */
    public function validate()
    {
        return Validator::make($this->params, $this->rules(), $this->errorMessages())->validate();
    }

    public function errorMessages()
    {
        return $this->errorMessages;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function fillModels()
    {
        foreach ($this->fillableModels as $model => $primaryKey) {
            if (property_exists($this, $model) && isset($this->params[$primaryKey])) {
                $this->$model = $this->$model->withoutGlobalScopes()->findOrFail($this->params[$primaryKey]);
            }
        }
    }

    public function fillModel(Model $model, $fields = [], $exceptedFields = [])
    {
        $fieldsCollect = collect($fields);
        if ($exceptedFields) {
            $fieldsCollect = $fieldsCollect->except($exceptedFields);
        }
        foreach ($fieldsCollect as $key => $value) {
            if (array_key_exists($key, $model->getAttributes())) {
                $model->$key = $value;
            }
        }
    }
}
