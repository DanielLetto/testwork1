<?php

namespace App\Services\Category;

use App\Models\Category;
use App\Services\BaseService;

class SCategoryCreate extends BaseService
{
    protected $Category;

    /**
     * SProductCreate constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->Category = $category;
    }

    /**
     * @return array|string[]
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:12',
            'eid' => 'required|integer',
        ];
    }

    public function handle()
    {
        $this->Category->title = $this->params['title'];
        $this->Category->eid = $this->params['eid'];
        $this->Category->save();

        $this->result = $this->Category;
    }

    /**
     * @return Category
     */
    public function getResult(): Category
    {
        return $this->result;
    }
}
