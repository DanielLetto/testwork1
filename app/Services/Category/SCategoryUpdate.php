<?php

namespace App\Services\Category;

use App\Models\Category;
use App\Services\BaseService;

class SCategoryUpdate extends BaseService
{
    protected $Category;

    /**
     * SProductCreate constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->Category = $category;
    }

    protected $fillableModels = [
        'Category' => 'id'
    ];

    /**
     * @return array|string[]
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:categories,id',
            'title' => 'nullable|string|min:3|max:12',
            'eid' => 'nullable|integer',
        ];
    }

    public function handle()
    {
        $this->Category->title = $this->params['title'] ?? $this->Category->title;
        $this->Category->eid = $this->params['eid'] ?? $this->Category->eid;
        $this->Category->save();

        $this->result = $this->Category;
    }

    /**
     * @return Category
     */
    public function getResult(): Category
    {
        return $this->result;
    }
}
