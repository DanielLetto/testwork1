<?php

namespace App\Services\Category;

use App\Models\Category;
use App\Models\Product;
use App\Services\BaseService;
use Exception;

class SCategoryDelete extends BaseService
{
    protected $Category;

    /**
     * SProductCreate constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->Category = $category;
    }

    protected $fillableModels = [
        'Category' => 'id'
    ];

    /**
     * @return array|string[]
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:categories,id',
        ];
    }

    public function handle()
    {
        $this->Category->products()->detach();
        $this->Category->delete();

        $this->result = $this->Category;
    }

    /**
     * @return Category
     */
    public function getResult(): Category
    {
        return $this->result;
    }
}
