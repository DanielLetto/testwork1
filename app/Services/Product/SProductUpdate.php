<?php

namespace App\Services\Product;

use App\Events\EProductUpdate;
use App\Models\Product;
use App\Services\BaseService;

class SProductUpdate extends BaseService
{
    protected $Product;

    /**
     * SProductUpdate constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->Product = $product;
    }

    /**
     * @var string[]
     */
    protected $fillableModels = [
        'Product' => 'id'
    ];

    /**
     * @return array|string[]
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:products,id',
            'category_ids' => 'nullable|array',
            'category_ids.*' => 'exists:categories,id',
            'title' => 'nullable|string|min:3|max:12',
            'price' => 'nullable|min:0|max:200',
            'eid' => 'nullable|integer',
        ];
    }


    public function handle()
    {
        $this->Product->title = $this->params['title'] ?? $this->Product->title;
        $this->Product->price = $this->params['price'] ?? $this->Product->price;
        $this->Product->eid = $this->params['eid'] ?? $this->Product->eid;
        $this->Product->save();

        if (isset($this->params['category_ids'])) {
            $this->Product->categories()->sync($this->params['category_ids']);
        }

        $this->result = $this->Product;

        event(new EProductUpdate($this->Product));
    }

    /**
     * @return Product
     */
    public function getResult(): Product
    {
        return $this->result;
    }
}
