<?php

namespace App\Services\Product;

use App\Models\Product;
use App\Services\BaseService;
use Exception;

class SProductDelete extends BaseService
{
    protected $Product;

    /**
     * SCourseDelete constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->Product = $product;
    }

    /**
     * @var string[]
     */
    protected $fillableModels = [
        'Product' => 'id'
    ];

    /**
     * @return array|string[]
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:products,id',
        ];
    }

    /**
     * @throws Exception
     */
    public function handle()
    {
        $this->Product->categories()->detach();
        $this->Product->delete();
        $this->result = $this->Product;
    }

    /**
     * @return Product
     */
    public function getResult(): Product
    {
        return $this->result;
    }
}
