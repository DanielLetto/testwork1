<?php

namespace App\Services\Product;

use App\Events\EProductCreate;
use App\Models\Product;
use App\Services\BaseService;

class SProductCreate extends BaseService
{
    protected $Product;

    /**
     * SProductCreate constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->Product = $product;
    }

    /**
     * @return array|string[]
     */
    public function rules()
    {
        return [
            'category_ids' => 'required|array',
            'category_ids.*' => 'exists:categories,id',
            'title' => 'required|string|min:3|max:12',
            'price' => 'required|min:0|max:200',
            'eid' => 'required|integer',
        ];
    }

    public function handle()
    {
        $this->Product->title = $this->params['title'];
        $this->Product->price = $this->params['price'];
        $this->Product->eid = $this->params['eid'];
        $this->Product->save();

        $this->Product->categories()->sync($this->params['category_ids']);

        $this->result = $this->Product;

        event(new EProductCreate($this->Product));
    }

    /**
     * @return Product
     */
    public function getResult(): Product
    {
        return $this->result;
    }
}
