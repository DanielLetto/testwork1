<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Resources\RCategory;
use App\Models\Category;
use App\Services\Category\SCategoryCreate;
use App\Services\Category\SCategoryUpdate;
use App\Services\Category\SCategoryDelete;
use Illuminate\Http\Request;

class CCategory extends Controller
{
    protected $Category;

    public function __construct(Category $category)
    {
        $this->Category = $category;
    }

    public function list()
    {
        $categories = $this->Category->query()
            ->with('products')
            ->get();
//        return $categories;
        return RCategory::collection($categories);
    }

    public function create(Request $request, SCategoryCreate $categoryCreate)
    {
        $categoryCreate->init($request->all())->handle();
        return response()->json(['message' => true]);
    }

    public function update(Request $request, SCategoryUpdate $categoryUpdate)
    {
        $categoryUpdate->init($request->all())->handle();
        return response()->json(['message' => true]);
    }

    public function delete(Request $request, SCategoryDelete $categoryDelete)
    {
        $categoryDelete->init($request->all())->handle();
        return response()->json(['message' => true]);
    }
}
