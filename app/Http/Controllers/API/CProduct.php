<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\RProduct;
use App\Models\Product;
use App\Services\Product\SProductCreate;
use App\Services\Product\SProductDelete;
use App\Services\Product\SProductUpdate;
use Illuminate\Http\Request;

class CProduct extends Controller
{
    protected $Product;

    public function __construct(Product $product)
    {
        $this->Product = $product;
    }

    public function list()
    {
        $products = $this->Product->query()
            ->with('categories')
            ->get();
        return RProduct::collection($products);
    }

    public function create(Request $request, SProductCreate $productCreate)
    {
        $productCreate->init($request->all())->handle();
        return response()->json(['message' => true]);
    }

    public function update(Request $request, SProductUpdate $productUpdate)
    {
        $productUpdate->init($request->all())->handle();
        return response()->json(['message' => true]);
    }

    public function delete(Request $request, SProductDelete $productDelete)
    {
        $productDelete->init($request->all())->handle();
        return response()->json(['message' => true]);
    }
}
