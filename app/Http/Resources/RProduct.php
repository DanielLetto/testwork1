<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class RProduct extends JsonResource
{
    /** @var Product */
    public $resource;

    public function toArray($request)
    {
        return [
            'title' => $this->resource->title,
            'price' => $this->resource->price,
            'eid' => $this->resource->eid,
            'categories_detail' => RCategory::collection($this->whenLoaded('categories')),
        ];
    }
}
