<?php


namespace App\Http\Resources;


use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class RCategory extends JsonResource
{
    /** @var Category */
    public $resource;

    public function toArray($request)
    {
        return [
            'title' => $this->resource->title,
            'eid' => $this->resource->eid,
            'product_detail' => RProduct::collection($this->whenLoaded('products')),
        ];
    }
}
