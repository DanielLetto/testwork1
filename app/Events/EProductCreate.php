<?php

namespace App\Events;

use App\Models\Product;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EProductCreate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $Product;

    public function __construct(Product $product)
    {
        $this->Product = $product;
    }

    public function broadcastOn()
    {
        return true;
    }
}
