Сущности
> [app\Models](app\Models)

Ресурсы
> [app\Http\Resources](app\Http\Resources)

Одна сущность - одна папка 
> [\App\Services](\App\Services)

Настройки путей для чтения json файлов 
> [config/core.php](config/core.php)

Комманды на чтение из файлов json (добавил простое логирование данных при ошибке валидации)
> файлы должны находиться в app/storage/json

> [\App\Console\Commands\CategoriesJson.php](\App\Console\Commands\CategoriesJson.php) (signature = categories:createOrUpdate)
>
> [\App\Console\Commands\ProductsJson.php](\App\Console\Commands\ProductsJson.php) (signature = product:createOrUpdate)

events на Product с отправкой почты запускаются из сервисов 
> [\App\Services\Product\SProductCreate.php](App\Services\Product\SProductCreate.php)
>
> [\App\Services\Product\SProductUpdate.php](\App\Services\Product\SProductUpdate.php)

MAIL_DRIVER=log
> [.env](.env)

Все Api маршруты
> [routes/api.php](routes/api.php)

---

### storage\json

> category.json
```
[
  {
    "eId": 1,
    "title": "Category 1"
  },
  {
    "eId": 2,
    "title": "Category 2"
  },
  {
    "eId": 2,
    "title": "Category 33333333"
  }
]
```

> product.json
```
[
[
  {
    "eId": 1,
    "title": "Product 1",
    "price": 101.01,
    "category_ids": [
      1,
      2
    ]
  },
  {
    "eId": 2,
    "title": "Product 2",
    "price": 199.01,
    "category_ids": [
      1,
      2
    ]
  },
  {
    "eId": 3,
    "title": "Product 33333333",
    "price": 999.01,
    "category_ids": [
      3,
      5
    ]
  }
]
]
```



