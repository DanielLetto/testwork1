<?php

use App\Http\Controllers\API\CProduct;
use App\Http\Controllers\Api\CCategory;


Route::get('/category/list', [CCategory::class, 'list']);
Route::post('/category/create', [CCategory::class, 'create']);
Route::post('/category/update', [CCategory::class, 'update']);
Route::post('/category/delete', [CCategory::class, 'delete']);

Route::get('/product/list', [CProduct::class, 'list']);
Route::post('/product/create', [CProduct::class, 'create']);
Route::post('/product/update', [CProduct::class, 'update']);
Route::post('/product/delete', [CProduct::class, 'delete']);




